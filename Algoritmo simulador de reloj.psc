Algoritmo A7
	//hacer un diagrama de flujo que simula un reloj
	para horas<-0 hasta 12
		Para minutos<-0 hasta 60
			para segundos<-0 hasta 60
				para milisegundos<-0 hasta 5
					//escribir horas,":",minutos,":",segundos,":",milisegundos
					para k<-1 Hasta 240000
					FinPara
				FinPara
				Escribir horas, ":",minutos,":",segundos
				//no se puden poner milisegundos, sino el reloj empieza a contar de 10 en 10,
				//reemplazando los digitos del milisegundos en los segundos.
			FinPara
		FinPara
	FinPara
	//no entiendo por que tiene que ser hasta 200000
	//es hasta 240000 porque es el horario de 1 dia
FinAlgoritmo
