Algoritmo A6
	//la sucesion fibonnaci se define de la siguente forma a(1)=1, a(2)=1 y a(n)=a(n-1)+a(n-2) para n>2, es decir los dos primeros son 1
	//y el resto cada uno es la suma de los dos anteriores, los primeros numeros de la sucesion son 1,1,2,3,5,8,13,21... hacer un
	//diagrama de flujo para calcular el n-esimo termino de la sucesion
		N<-0;
		X<-1;
		Escribir "Ingresar Numero";
		NUMERO<-0;
		Leer NUMERO;
		PARA I<-0 HASTA NUMERO CON PASO 1 Hacer
			Escribir N;
			A<-X+N
			N<-X
			X<-A
		FinPara
FinProceso

