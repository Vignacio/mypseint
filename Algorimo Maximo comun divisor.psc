Algoritmo A9
	//hacer un diagrama de flujo para calcular el maxico comun divisor de dos numeros enteros positivos N y M
	//siguiendo el diagrama de eucludes, que es el siguente
	//a)se define N por M, sea R el resto
	//b)si R=0, al maximo comun divisor es M y se acaba
	//c)se designa a N el valor de M y a M el valor de R y volver al paso a)
	Definir num1,num2,a,b,res Como Entero
    // Pedimos al usuario que ingrese los datos
    Escribir 'Ingrese el primer numero'
    Leer num1
    Escribir 'Ingrese el segundo numero'
    Leer num2
    // Seleccionamos el mayor y el menor para
    // asignarlos a las variables "a" y "b"
    Si num1>num2 Entonces
        a<-num1
        b<-num2
    Sino
        a<-num2
        b<-num1
    FinSi
    // Hacemos el ciclo encargado de
    // realizar las iteraciones
    Mientras b!=0 Hacer
        res<-b
        b<-a MOD b
        a<-res
    FinMientras
    // Mostramos el resultado en pantalla
    Escribir 'El M.C.D. entre ',num1,' y ',num2,' es: ',res	
FinAlgoritmo