Proceso A10
	//hacer un diagrama de flujo para calcular al factorial de N (N!=1*2*3*4*5*...*N)
    acum<-1;

    Escribir "Digite un numero para calcular su factorial";
	
    Leer num;
	
    Para i<-1 Hasta num Con Paso 1 Hacer
		
        acum<-acum*i;
		
    FinPara
	
    Escribir "El factorial de ",num," es ",acum;
	// fuente de este algoritmo http://pseintlover.blogspot.com/2012/07/factorial-de-un-numero-para.html
	//factoriales son los numeros que se multiplican tantas veces numeros tentan antecesorres a ellos, este algoritmo cumple su funcion
FinProceso