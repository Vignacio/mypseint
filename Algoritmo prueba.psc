Algoritmo Liquidacion_de_sueldo
	//Se requiere determinar el las imposiciones mensuales de un trabajador con base en las horas que trabaja y el pago por hora que recibe.
	//Tomando en cuenta que las imposiciones de este trabajador estan conformadas por un 10% destinado al pago de AFP y un 7% al pago de Salud.
	//Realice el diagrama de flujo y el pseudocódigo que representen el algoritmo de solución correspondiente.
	//teniendo en cuanta el sueldo minimo $288.000
	//teniendo en cuanta 8 horas de trabajo diario
	//teniendo en cuenta 20 dias de trabajo
	//teniendo en cuenta $1800 por hora
	//descuentos 10% AFP,7% SALUD
	//teniendo en cuenta por semana 40 horas

	Definir AFP, SALUD como real;
	Definir salario, sueldo, descuentos, pago_por_hora como real;
	
	Escribir "horas de trabajo ";
	Leer horas
	Escribir "pago por hora ";
	Leer pago_por_hora
	
//	AFP = 10%
//	SALUD = 7%
	salario = horas*pago_por_hora
	AFP = salario*0.10
	SALUD = salario*0.07
	
	descuentos = AFP+SALUD;
	sueldo = salario-descuentos
	
	Escribir "total de descuentos aplicados: ", descuentos;
	Escribir "total salario neto: " sueldo;
	//los resultados aparecen con un "0" demas, ya que, el algoritmo es en funcion a una multiplicacion de decimales
FinAlgoritmo
